# gmail-sys-api

System API that connects to GMail using credentials provided.

- requires "mule_env" environment variable to be set to dev to run in Anypoint Studio.
- add your credentials to the src/main/resources/dev.properties file to run the API